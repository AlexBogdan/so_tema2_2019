CC = gcc
CFLAGS = -Wall -fPIC

LDFLAGS = -L.

build:  libso_stdio.so

libso_stdio.so: main.o
	$(CC) -shared -o $@ $^

main.o: main.c

clean:
	rm -f *.o libso_stdio.so
