/* Autor : Penitent Tangent - 2401 Monitor  */

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#include "util/so_stdio.h"

#define BUF_SIZE 4096
#define NO_OP 0
#define READ_OP 1
#define WRITE_OP 2

/* Ne definim modurile in care putem deschide fisierul */
#define MODE_READ 1
#define MODE_READ_PLUS 2
#define MODE_WRITE 3
#define MODE_WRITE_PLUS 4
#define MODE_APPEND 5
#define MODE_APPEND_PLUS 6


/* Pastram in structura SO_FILE un file_descriptor pentru fisierul deschis */
struct _so_file {
	int fd;
	unsigned char *buffer;
	/* Tinem minte cate caractere avem in momentul de fata in buffer */
	int read_limit;
	/* Tinem minte la ce caracter am ajuns cu citirea in buffer */
	int read_index;
	int write_index;
	/* Tinem minte ultima operatie efectuata pe fisier (READ / WRITE) */
	int last_op;
	int error;
	long cursor_pos;
	int is_popen;
	pid_t pid;
};

/*
 * Constructor pentru structura definita mai sus.
 * Asteptam sa primim file_descriptorul ce trebuie salvat.
 * In cazul in care primim si un pid, fisierul a fost deschis de so_popen
 */
SO_FILE *create_so_file(const int fd, pid_t pid)
{
	SO_FILE *stream;

	/* Daca am primit un fd invalid, iesim */
	if (fd == -1)
		return NULL;

	/* Alocam o structura SO_FILE */
	stream = malloc(sizeof(SO_FILE));
	if (stream == NULL)
		return NULL;

	/* Incercam sa alocam bufferul intern structurii */
	stream->buffer = (unsigned char *)
			calloc(BUF_SIZE, sizeof(unsigned char));
	if (stream->buffer == NULL) {
		free(stream);
		return NULL;
	}
	stream->fd = fd;

	/*
	 *	Initial nu avem nicio operatie facuta pe fisier,
	 * deci invalidam toti indexii
	 */
	stream->read_limit = -1;
	stream->read_index = -1;
	stream->write_index = -1;
	stream->last_op = NO_OP;
	stream->error = 0;
	stream->cursor_pos = 0;

	stream->pid = pid;
	/* Verificam daca procesul a fost deschis cu poepn */
	if (pid == -1)
		stream->is_popen = 0;
	else
		stream->is_popen = 1;

	return stream;
}

/*
 * Parsam modul primit ca parametru pentru deschiderea unui fisier
 */
int parse_mode(const char *mode)
{
	if (strcmp(mode, "r") == 0)
		return MODE_READ;
	if (strcmp(mode, "r+") == 0)
		return MODE_READ_PLUS;
	if (strcmp(mode, "w") == 0)
		return MODE_WRITE;
	if (strcmp(mode, "w+") == 0)
		return MODE_WRITE_PLUS;
	if (strcmp(mode, "a") == 0)
		return MODE_APPEND;
	if (strcmp(mode, "a+") == 0)
		return MODE_APPEND_PLUS;

	return -1;
}

/*
 * Stabilim modul in care va fi deschis fisierul
 */
int select_mode(const char *mode)
{
	switch (parse_mode(mode)) {
	case MODE_READ:
		return O_RDONLY;
	case MODE_READ_PLUS:
		return O_RDWR;
	case MODE_WRITE:
		return O_WRONLY | O_CREAT | O_TRUNC;
	case MODE_WRITE_PLUS:
		return O_RDWR | O_CREAT | O_TRUNC;
	case MODE_APPEND:
		return O_WRONLY | O_APPEND | O_CREAT;
	case MODE_APPEND_PLUS:
		return O_RDWR | O_APPEND | O_CREAT;
	default:
		return -1;
	}
}

SO_FILE *so_fopen(const char *pathname, const char *mode)
{
	SO_FILE *stream;
	int fd, mod;

	mod = parse_mode(mode);

	/* Incercam sa deschidem fisierul */
	if (mod == MODE_READ || mod == MODE_READ_PLUS)
		fd = open(pathname, select_mode(mode));
	else
		/* In caz ca trebuie sa cream fisierul, avem si permisiuni */
		fd = open(pathname, select_mode(mode), 0644);

	if (fd == -1)
		return NULL;

	stream = create_so_file(fd, -1);

	return stream;
}

/*
 * Destructor pentru SO_FILE
 */
int close_file(SO_FILE *stream)
{
	int rc;

	rc = close(stream->fd);
	free(stream->buffer);
	free(stream);

	return rc;
}

int so_fclose(SO_FILE *stream)
{
	int rc;

	/*  Inainte de a inchide fisierul, facem fflush
	 * in cazul in care am avut o operatie de scriere inainte
	 */
	if (stream->last_op == WRITE_OP)
		if (so_fflush(stream) == SO_EOF) {
			close_file(stream);

			return SO_EOF;
		}

	/* Eliberam memoria alocata pentru SO_FILE */
	rc = close_file(stream);

	if (rc == 0)
		return 0;

	return SO_EOF;
}

int read_in_buffer(SO_FILE *stream)
{
	int read_bytes;

	/* Setam buffer-ul pe 0 inainte de a-l popula */
	memset(stream->buffer, 0, BUF_SIZE);

	/* Citim datele din fisier in buffer */
	read_bytes = (int) read(stream->fd, stream->buffer, BUF_SIZE);

	if (read_bytes == -1) {
		stream->error = SO_EOF;
		return SO_EOF;
	}

	/*	Daca buffer-ul nu a fost umplut,
	 * marcam primul byte disponibil cu EOF
	 */
	if (read_bytes < BUF_SIZE)
		stream->buffer[read_bytes] = SO_EOF;

	/* Retinem cate caractere au fost citite in buffer */
	stream->read_limit = read_bytes;
	/* Resetam cursorul bufferului pentru datele noi */
	stream->read_index = 0;
	stream->write_index = -1;

	return read_bytes;
}

int so_fgetc(SO_FILE *stream)
{
	int rc = 0;
	unsigned char c;

	/*  Daca bufferul nu are date disponibile pentru citire sau
	 * s-a depasit limita de caractere, repopulam buffer-ul
	 */
	if (stream->last_op != READ_OP ||
		stream->read_limit - stream->read_index <= 0
		) {
		rc = read_in_buffer(stream);
		if (stream->read_limit == 0)
			return SO_EOF;
	}

	/* Ne asiguram ca nu am avut nicio problema in momentul citirii */
	if (rc == SO_EOF)
		return SO_EOF;

	/* Citim caracterul si mutam cursorul */
	c = stream->buffer[stream->read_index];
	stream->read_index++;
	stream->cursor_pos++;
	stream->last_op = READ_OP;

	return (int) c;
}

size_t so_fread(void *ptr, size_t size, size_t nmemb, SO_FILE *stream)
{
	unsigned char *buffer;
	int chunk, index, c;

	/* Alocam structura in care vom citi datele */
	buffer = (unsigned char *) calloc(nmemb * size, sizeof(unsigned char));
	if (buffer == NULL) {
		stream->error = SO_EOF;
		return 0;
	}

	/* Cat timp putem, citim datele*/
	for (chunk = 0; chunk < nmemb; chunk++)
		for (index = 0; index < size; index++) {
			c = so_fgetc(stream);
			if (c != SO_EOF)
				buffer[chunk * size + index] =
						(unsigned char) c;
			else {
				/* Copiem datele citite la adresa ptr */
				memcpy(ptr, buffer, chunk * size + index);
				free(buffer);
				return chunk;
			}
		}

	/* Copiem datele citite la adresa ptr */
	memcpy(ptr, buffer, size * nmemb);
	free(buffer);

	return nmemb;
}

int so_fflush(SO_FILE *stream)
{
	int written_bytes;

	/* Scriem datele in fisier */
	written_bytes = (int) write(
			stream->fd,
			stream->buffer,
			stream->write_index
	);

	if (written_bytes == -1) {
		stream->error = SO_EOF;
		return SO_EOF;
	}

	/* Curatam buffer-ul */
	memset(stream->buffer, 0, BUF_SIZE);
	/* Resetam cursorul bufferului pentru a scrie de la inceput */
	stream->write_index = 0;
	stream->read_index = -1;

	return 0;
}

int so_fputc(int c, SO_FILE *stream)
{
	int rc = 0;

	if (stream->write_index == -1)
		stream->write_index = 0;

	/* In cazul in care bufferul este plin, facem fflush */
	if (stream->write_index == BUF_SIZE)
		rc = so_fflush(stream);

	/* Verificam daca nu am avut probleme in momentul scrierii datelor */
	if (rc == SO_EOF)
		return SO_EOF;

	/* Salvam caracterul in buffer si incrementam indexii */
	stream->buffer[stream->write_index] = (unsigned char) c;
	stream->cursor_pos++;
	stream->write_index++;
	stream->last_op = WRITE_OP;

	return c;
}

size_t so_fwrite(const void *ptr, size_t size, size_t nmemb, SO_FILE *stream)
{
	int chunk, index, c;

	unsigned char *text = (unsigned char *) ptr;

	/* Scriem cate caractere putem */
	for (chunk = 0; chunk < nmemb; chunk++)
		for (index = 0; index < size; index++) {
			c = so_fputc((int) text[chunk * size + index], stream);
			if (c == SO_EOF) {
				stream->error = SO_EOF;
				return chunk;
			}
		}

	return nmemb;
}

int so_fseek(SO_FILE *stream, long offset, int whence)
{
	off_t rc = 0;

	/*	Daca ultima operatie a fost una de scriere,
	 * atunci scriem datele din buffer
	 */
	if (stream->last_op == WRITE_OP) {
		rc = so_fflush(stream);
		if (rc == -1) {
			stream->error = SO_EOF;
			return -1;
		}
	}

	/* Invalidam buffer-ul */
	memset(stream->buffer, 0, BUF_SIZE);
	stream->read_index = -1;
	stream->write_index = -1;
	stream->read_limit = -1;
	stream->last_op = NO_OP;

	/* Mutam cursorul in fisierul fizic */
	rc = lseek(stream->fd, offset, whence);

	if (rc == -1) {
		stream->error = SO_EOF;
		return -1;
	}

	stream->cursor_pos = rc;
	return 0;
}

long so_ftell(SO_FILE *stream)
{
	return stream->cursor_pos;
}

int so_fileno(SO_FILE *stream)
{
	return stream->fd;
}

int so_feof(SO_FILE *stream)
{
	/* Daca in buffer avem doar SO_EOF, suntem la finalul fisierului */
	if (stream->read_limit == 0)
		return 1;
	return 0;
}

int so_ferror(SO_FILE *stream)
{
	return stream->error;
}

/* Vom executa o comanda de forma "sh" "-c" command */
char **parse_command(const char *command)
{
	char **args;

	args = (char **) malloc(4 * sizeof(char *));
	args[0] = "sh";
	args[1] = "-c";
	args[2] = (char *) command;
	args[3] = NULL;

	return args;
}

SO_FILE *so_popen(const char *command, const char *type)
{
	pid_t child_pid;
	int rc, mode;
	SO_FILE *stream;
	char **args;
	/* Descriptorii de fisiere pentru pipe */
	int pipe_fd[2];

	mode = parse_mode(type);
	if (!(mode == MODE_READ || mode == MODE_WRITE))
		return NULL;

	/* Cream un pipe */
	rc = pipe(pipe_fd);
	if (rc == -1)
		return NULL;

	/* Cream un nou proces care sa execute comanda */
	args = parse_command(command);
	child_pid = fork();

	switch (child_pid) {
	case -1: /* In caz de eroare, iesim direct */
		return NULL;
	case 0: /* Procesul copil */
		/* Verificam ce capat al pipe-ului ne intereseaza */
		if (mode == MODE_READ)
			rc = dup2(pipe_fd[1], STDOUT_FILENO);
		else
			rc = dup2(pipe_fd[0], STDIN_FILENO);

		/* Inchidem pipe-ul din partea procesului copil */
		close(pipe_fd[0]);
		close(pipe_fd[1]);

		if (rc == -1)
			exit(rc);

		/* Executam comanda parsata */
		execvp(args[0], args);

		/* In cazul in care am avut o problema, iesim cu fail */
		exit(-1);
	default: /*Procesul parinte (curent) */
		/* Cream un nou SO_FILE in care vom avea ca file_descriptor
		 * capatul de pipe pastrat deschis de catre proces
		 */
		if (mode == MODE_READ) {
			stream = create_so_file(pipe_fd[0], child_pid);
			close(pipe_fd[1]);
		} else {
			stream = create_so_file(pipe_fd[1], child_pid);
			close(pipe_fd[0]);
		}
		free(args);

		return stream;
	}
}

int so_pclose(SO_FILE *stream)
{
	int status, rc;
	pid_t wait_rc;

	/* Verificam daca fisierul a fost deschis cu so_popen */
	if (stream->is_popen == 0) {
		stream->error = 1;
		return -1;
	}

	/* Salvam pid-ul procesului copil care inca ruleaza */
	pid_t p = stream->pid;

	/* Inchidem SO_FILE-ul, deci si capatul de pipe ramas deschis */
	rc = so_fclose(stream);
	if (rc == -1)
		return -1;

	/* Asteptam ca procesul sa se termine */
	wait_rc = waitpid(p, &status, 0);
	if (wait_rc < 0)
		return -1;

	return status;
}
